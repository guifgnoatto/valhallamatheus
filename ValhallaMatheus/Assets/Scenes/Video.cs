﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Video : MonoBehaviour
{
    Gerenciador g;
    GameObject minhaCamera;
    public float velocidade =.1f;
    // Start is called before the first frame update
    void Start()
    {
        g = GetComponent<Gerenciador>();
        minhaCamera = Camera.main.gameObject;
        g.carta1 = "Guerreiro 1";
        g.carta2 = "Mago 2";
        g.carta3 = "Arqueiro 1";
    }
    private void FixedUpdate()
    {
        minhaCamera.transform.position += (Vector3.back) * velocidade;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            g.EnviarMinhasCartas(true);
        }
    }
}
