﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class LancarDados : MonoBehaviour
{
    public GameObject dado1, dado2, dado3;
    public int dadosParaJogar, delaySegundos;
    // Start is called before the first frame update
    void Start()
    {
        //dadosParaJogar = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            JogarDados();
        }
    }
    public async void  JogarDados()
    {
        for (int i = dadosParaJogar; i > 0; i--)
        {
            Instantiate(dado2, gameObject.transform.position, gameObject.transform.rotation);

            await Task.Delay(delaySegundos * 1000);
        }
    }
}
