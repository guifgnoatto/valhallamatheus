﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dados : MonoBehaviour
{
    public Rigidbody rigidbody;
    public int myNumber;
    public float myTimeRolling;
    public bool diceIsRolled;
    public Renderer myRenderer;
    public Material[] numbers;
    public GameObject[] myLights;
    public Light light;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody.AddForce(transform.forward * 50);
        myNumber = Random.Range(1, 6);
        myRenderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {


        myTimeRolling += Time.deltaTime;
        if (myTimeRolling < 4.5f)
        {
                //light.range = myTimeRolling;
                
        
        }
        else
        {
             myRenderer.material = numbers[myNumber - 1];
        }
      
           
        
    }
}
   

