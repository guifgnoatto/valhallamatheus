﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LunaEffects : MonoBehaviour
{
    public GameObject flameTrower, fireBall, defense, hittedBlood, diedBlood;
    // Start is called before the first frame update
    void Start()
    {
        flameTrower.SetActive(false);
        fireBall.SetActive(false);
        defense.SetActive(false);
        hittedBlood.SetActive(false);
        diedBlood.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ActiveFlameTrower()
    {
        flameTrower.SetActive(true);
    }
    public void DesactiveFlameTrower()
    {
        flameTrower.SetActive(false);
    }
    public void ActiveFireball()
    {
        fireBall.SetActive(true);
    }
    public void DesactiveFireball()
    {
        fireBall.SetActive(false);
    }
    public void ActiveDefense()
    {
        defense.SetActive(true);
    }
    public void DesactiveDefense()
    {
        defense.SetActive(false);
    }
    public void ActiveHittedblood()
    {
        hittedBlood.SetActive(true);
    }
    //public void ActiveFlameTrower()
    //{
    //    flameTrower.SetActive(true);
    //}
    public void ActiveDiedBlood()
    {
        diedBlood.SetActive(true);
    }
}
