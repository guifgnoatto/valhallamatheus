﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodEffects : MonoBehaviour
{
    public GameObject  hittedBlood, diedBlood;
    // Start is called before the first frame update
    void Start()
    {
        DesactivateEverything();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DesactivateEverything()
    {
        hittedBlood.SetActive(false);
        diedBlood.SetActive(false);
    }

    public void ActivateHittedBlood()
    {
        hittedBlood.SetActive(true);
    }

    public void ActivateDiedBlood()
    {
        diedBlood.SetActive(true);
    }
}
