﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DunhillEffects : MonoBehaviour
{
    public GameObject attackEffect, hittedBlood, diedBlood; 
    // Start is called before the first frame update
    void Start()
    {
        DesactivateEverything();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DesactivateEverything()
    {
        attackEffect.SetActive(false);
        hittedBlood.SetActive(false);
        diedBlood.SetActive(false);
    }

    public void ActivateAttack()
    {
        attackEffect.SetActive(true);
    }

    public void ActivateHittedBlood()
    {
        hittedBlood.SetActive(true);
    }

    public void ActivateDiedBlood()
    {
        diedBlood.SetActive(true);
    }
}
