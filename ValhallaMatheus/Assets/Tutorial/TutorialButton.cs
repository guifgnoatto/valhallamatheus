﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialButton : MonoBehaviour
{
    public GameObject[] tutorials;
    private int _actualTutorial = 0;

    public void NextTutorial()
    {        
        if(_actualTutorial + 2 > tutorials.Length)
        {  

            Debug.Log("deu");
            Destroy(gameObject);
        }
        else
        {
            tutorials[_actualTutorial].SetActive(false);
            _actualTutorial += 1;
            tutorials[_actualTutorial].SetActive(true);
        }
    }

    public void PularTutorial()
    {
        gameObject.SetActive(false);
    }
}
