﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Collections.Generic;

public class Client : MonoBehaviour
{
    public Baralho baralho;
    public string clientName;
    public Gerenciador gerenciador;

    private bool socketReady;
    private TcpClient socket;
    private NetworkStream stream;
    private StreamWriter writer;
    private StreamReader reader;

    public List<GameClient> players;

    [HideInInspector]
    public int[] poderes = new int[5];


    private void Start()
    {        
        players = new List<GameClient>();
        DontDestroyOnLoad(gameObject);
    }
    public bool ConnectToServer(string host, int port)
    {
        if (socketReady)
            return false;
        try
        {
            socket = new TcpClient(host, port);
            stream = socket.GetStream();
            writer = new StreamWriter(stream);
            reader = new StreamReader(stream);
            socketReady = true;
        }
        catch(Exception e)
        {
            Debug.Log("Socket Error: " + e.Message);
        }
        return socketReady;
    }

    private void Update()
    {
        //if (baralho == null)
        //    baralho = GameObject.FindObjectOfType<Baralho>();
        if (socketReady)
        {
            if (stream.DataAvailable)
            {
                string data = reader.ReadLine();
                Debug.Log("DATA: " + data);
                if (data != null)
                    OnIncomingData(data);
            }
        }

        //if (Input.GetMouseButtonDown(0))
        //{
        //    Debug.Log(players[0].name + "  " + players[0].isHost);
        //    Debug.Log(players.Count);
        //}
    }
    //Mandar mensagens para o server
    public void Send (string data)
    {
        //if (!socketReady)
        //    return;
        while (!socketReady)
        {
            int i = 1;
        }
        writer.WriteLine(data);
        writer.Flush();
    }
    //Ler mensagens do server
    private void OnIncomingData(string data)
    {
        Debug.Log("Client recebeu: " + data);
        string[] aData = data.Split('|');

        switch (aData[0])
        {
            case "SWHO":
                for (int i = 1; i < aData.Length - 1; i++)
                {
                    UserConnected(aData[i], false);
                }
                Send("CWHO|" + clientName);
                break;
            case "SCNN":
                if (aData[1] == "Host")
                {
                    Debug.Log("Host entrou");
                    UserConnected(aData[1], true);
                }
                else
                    UserConnected(aData[1], false);
                if (aData[2].ToUpper() == "TRUE")
                {
                    SceneManager.LoadScene("Arena");
                }
                break;
            case "PODERES":
                for (int i = 0; i < 5; i++)
                {
                    poderes[i] = Convert.ToInt32(aData[i + 1]);
                    Debug.Log("poder: " + poderes[i]);
                }
                break;
            case "TURNO":
                gerenciador.RecebeTurno(Convert.ToInt32(aData[1]));
                break;
            case "MYCARDS":
                Debug.Log("CLIENTE RECEBEU MYCARDS" + aData.Length);
                gerenciador.cartaI1 = aData[1];
                gerenciador.cartaI2 = aData[2];
                gerenciador.cartaI3 = aData[3];
                if (gerenciador.confirmados != 2)
                {
                    gerenciador.confirmados += 1;
                    gerenciador.ReceberCartasInimigo();
                }
                Send("READY|");
                break;
            case "FIMTURNO":
                gerenciador.RecebeTurno();
                break;
            case "ATACADO":
                gerenciador.RecebeCombate(aData[1], aData[2], Convert.ToInt32(aData[3]), Convert.ToInt32(aData[4]), Convert.ToInt32(aData[5]));
                gerenciador.RecebeTurno();
                break;
            case "RECUPERARVIDA":
                gerenciador.RecebeMagiaVida(Convert.ToInt32(aData[1]), Convert.ToInt32(aData[2]), Convert.ToInt32(aData[3]));
                break;
            case "DADOS":
                break;
            case "SELECIONARATACANTE":
                gerenciador.ReceberAtacante(aData[1]);
                break;
            case "NAOSELECIONARATACANTE":
                gerenciador.ReceberAtacante(aData[1],false);
                break;
            case "SELECIONARATACADA":
                gerenciador.ReceberAtacada(aData[1]);
                break;
            case "NAOSELECIONARATACADA":
                gerenciador.ReceberAtacada(aData[1], false);
                break;
            case "REVIVERPERSONAGEM":
                gerenciador.ReviverInimigo(aData[1]);
                break;
            case "DANOINSTANTANEO":
                gerenciador.ReceberDanoInstantaneo(aData[1]);
                break;
        }
    }
    private void UserConnected(string name, bool host)
    {
        GameClient c = new GameClient(name, host);
        players.Add(c);
        //GameObject.FindObjectOfType<Baralho>().GetComponent<Text>().text = players.Count.ToString();
        //if (players.Count == 2)
        //{
        //    SceneManager.LoadScene("Arena");
        //}
    }
    private void OnApplicationQuit()
    {
        CloseSocket();
    }
    private void OnDisable()
    {
        CloseSocket();
    }
    private void CloseSocket()
    {
        if (socketReady)
            return;
        writer.Close();
        reader.Close();
        socket.Close();
        socketReady = false;   
    }

}
public class GameClient
{
    public GameClient(string name, bool isHost)
    {
        this.name = name;
        this.isHost = isHost;
    }
    public string name;
    public bool isHost;
}


