﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotoesFuncoes : MonoBehaviour
{
    public GameObject menu;
    int width, height;
    bool fullscreen;
    private void Start()
    {
        fullscreen = false;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            AbrirMenu();
        }
    }
    public void AbrirMenu()
    {
        menu.SetActive(!menu.active);
    }
    public void Resolucao1280x720()
    {
        width = 1280;
        height = 720;
        MudarResolucao();
    }
    public void Resolucao1920x1080()
    {
        width = 1920;
        height = 1080;
        MudarResolucao();
    }
    public void Fullscreen()
    {
        fullscreen = !fullscreen;
        Screen.SetResolution(width, height, fullscreen);
    }
    void MudarResolucao()
    {
        Screen.SetResolution(width, height, fullscreen);
    }

    public void SairDoJogo()
    {
        Application.Quit();
    }

}
