﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
public class Server : MonoBehaviour
{
    public int port = 6321, turnoEnviadoPrimeiro = 0;

    private List<ServerClient> clients;
    private List<ServerClient> disconnectList;

    private TcpListener server;
    private bool serverStarted;

    int dado1, dado2;

    public void Init()
    {
        DontDestroyOnLoad(gameObject);
        clients = new List<ServerClient>();
        disconnectList = new List<ServerClient>();
        try
        {
            server = new TcpListener(IPAddress.Any, port);
            server.Start();

            StartListening();
        }
        catch (Exception e)
        {
            Debug.Log("Socket error: " + e.Message);
        }
    }

    private void Update()
    {
        if (!serverStarted)
            return;

        foreach (ServerClient c in clients)
        {
            //Cliente ainda está conectado?
            if (!IsConnected(c.tcp))
            {
                c.tcp.Close();
                disconnectList.Add(c);
                continue;
            }
            else
            { 
                NetworkStream s = c.tcp.GetStream();
                if (s.DataAvailable)
                {

                    StreamReader reader = new StreamReader(s, true);
                    string data = reader.ReadLine();
                    if (data != null)
                    {

                        OnIncomingData(c, data);
                    }
                }
            }
        }

        for (int i = 0; i < disconnectList.Count - 1; i++)
        {
            //Mostrar para o jogador se alguém desconectou

            clients.Remove(disconnectList[i]);
            disconnectList.RemoveAt(i);
        }
    }
    private void StartListening()
    {
        server.BeginAcceptTcpClient(AcceptTcpClient, server);
        serverStarted = true;
    }
    private void AcceptTcpClient(IAsyncResult ar)
    {
        TcpListener listener = (TcpListener)ar.AsyncState;


        ServerClient sc = new ServerClient(listener.EndAcceptTcpClient(ar));
        clients.Add(sc);
        StartListening();

        string allUsers = "";
        foreach (ServerClient i in clients)
        {
            allUsers += i.clientName + '|';
        }

        Broadcast("SWHO|" + allUsers, clients[clients.Count - 1]);
    }

    private bool IsConnected(TcpClient c)
    {
        try
        {
            if (c != null && c.Client != null && c.Client.Connected)
            {
                if (c.Client.Poll(0, SelectMode.SelectRead))
                    return !(c.Client.Receive(new byte[1], SocketFlags.Peek) == 0);
                return true;
            }
            else
                return false;
        }
        catch
        {
            return false;
        }
    }

    //Server send
    private void Broadcast(string data, List<ServerClient> cl)
    {
        foreach (ServerClient sc in cl)
        {
            try
            {
                StreamWriter writer = new StreamWriter(sc.tcp.GetStream());
                writer.WriteLine(data);
                writer.Flush();
            }
            catch (Exception e)
            {
                Debug.Log("Erro: " + e.Message);
            }
        }
    }
    private void Broadcast(string data, ServerClient c)
    {
        List<ServerClient> sc = new List<ServerClient> { c };
        Broadcast(data, sc);
    }
    //Server read
    private void OnIncomingData(ServerClient c, string data)
    {   
        Debug.Log("Server recebeu: " + data);
        string[] aData = data.Split('|');

        ServerClient oponente = clients[0];
        ServerClient jogador = clients[0];

        if (clients.Count > 1)
        {
            jogador = clients[1];
            oponente = clients[0];
            if (c.clientName == "Host")
            {
                jogador = clients[0];
                oponente = clients[1];
            }

        }
        switch (aData[0])
        {
            case "CWHO":
                c.clientName = aData[1];
                if (clients.Count > 1)
                {
                    Broadcast("SCNN|" + c.clientName + "|true", clients);
                }
                else
                {
                    Broadcast("SCNN|" + c.clientName + "|false", clients);
                }
                break;
            case "PODERES":
                if (c.clientName == "Host")
                    Broadcast("PODERES|" + aData[1] + "|" + aData[2] + "|" + aData[3] + "|" + aData[4] + "|" + aData[5], clients[1]);
                else
                    Broadcast("PODERES|" + aData[1] + "|" + aData[2] + "|" + aData[3] + "|" + aData[4] + "|" + aData[5], clients[0]);
                break;
            case "TURNO":
                if (c.clientName == "Host")
                {
                    Broadcast("TURNO|" + aData[1], clients[1]);
                }
                else
                    Broadcast("TURNO|" + aData[1], clients[0]);
                break;
            case "MYCARDS":
                Debug.Log("SERVER RECEBEU MYCARDS");
                if (c.clientName == "Host")
                    Broadcast("MYCARDS|" + aData[1] + "|" + aData[2] + "|" + aData[3], clients[1]);
                else
                    Broadcast("MYCARDS|" + aData[1] + "|" + aData[2] + "|" + aData[3], clients[0]);
                break;
            case "READY":
                int turnoEnviar;
                if (turnoEnviadoPrimeiro == 0)
                {
                    turnoEnviar = UnityEngine.Random.Range(1, 2);
                    turnoEnviadoPrimeiro = turnoEnviar;
                }
                else
                {
                    if (turnoEnviadoPrimeiro == 2)
                    {
                        turnoEnviar = 1;
                    }
                    else
                    {
                        turnoEnviar = 2;
                    }
                }
                Broadcast("TURNO|" + turnoEnviar, jogador);
                break;
            case "FIMTURNO":
                Broadcast("FIMTURNO|", oponente);
                break;
            case "ATAQUE":
                RodarDados(Convert.ToInt32(aData[2]),Convert.ToInt32(aData[4]));
                Broadcast("ATACADO|" + aData[1] + "|" + aData[3] + "|" + dado1 + "|" + dado2 + "|" + aData[5], clients);
                break;
            case "SELECIONARATACANTE":
                Broadcast("SELECIONARATACANTE|" + aData[1], oponente);
                break;
            case "NAOSELECIONARATACANTE":
                Broadcast("NAOSELECIONARATACANTE|" + aData[1], oponente);
                break;
            case "SELECIONARATACADA":
                Broadcast("SELECIONARATACADA|" + aData[1], oponente);
                break;
            case "NAOSELECIONARATACADA":
                Broadcast("NAOSELECIONARATACADA|" + aData[1], oponente);
                break;
            case "RECUPERARVIDA":
                Broadcast("RECUPERARVIDA|" + aData[1] + "|" + aData[2] + "|" +  aData[3], oponente);
                break;
            case "REVIVERPERSONAGEM":
                Broadcast("REVIVERPERSONAGEM|" + aData[1], oponente);
                break;
            case "DANOINSTANTANEO":
                Broadcast("DANOINSTANTANEO|" + aData[1], oponente);
                break;
        }
    }
    int RodarDados(int a, int b)
    {
        dado1 = UnityEngine.Random.Range(1, 6) * a;
        dado2 = UnityEngine.Random.Range(1, 6) * b;
        if (dado1 > dado2)
            return 1;
        else if (dado2 > dado1)
            return 2;
        else
            return 3;
    }
}

public class ServerClient
{
    public string clientName;
    public TcpClient tcp;

    public ServerClient(TcpClient tcp)
    {
        this.tcp = tcp;
    }
}