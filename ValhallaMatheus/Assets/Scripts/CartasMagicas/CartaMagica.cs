﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
//using UnityEditorInternal;
using UnityEngine;

public class CartaMagica : MonoBehaviour
{
    public int quantidade;
    float yReset;
    bool selecionada, selecionando;
    public bool sendoUsada;
    Vector3 posicaoNormal, posicaoSelecionada;
    public Gerenciador gerenciador;
    public string nome;
    public int magia;
    Carta cartaAfetada;
    public GameObject objSelecionada, usarEmQuem, materialMeu;
    public GameObject curarObj1, curarObj2, curarObj3, ressuscitarObj1, ressuscitarObj2, ressuscitarObj3;
    List<GameObject> cartasSobrando = new List<GameObject>();
    void Start()
    {
        sendoUsada = false;
        selecionada = false;
        yReset = transform.position.y;
        quantidade = 1;
    }
    void Update()
    {
        
    }
    public void setSendoUsada(bool s)
    {
        sendoUsada = s;
    }
    private void OnMouseDown()
    {
        if (gerenciador.confirmados != 3) //SELECIONANDO AS CARTAS
        {
            posicaoNormal = transform.position;
            posicaoNormal.y = yReset;
            if (!selecionada)
            {
                SelecionarDeck();
            }
            else
            {
                RetirarSelecaoDeck();
            }
        }
        else if (gerenciador.turno == gerenciador.meuTurno)
        {
            if (magia != 3 && magia != 4 && magia != 6)
                return;
            if (!sendoUsada)
            {
                gerenciador.receberCartaMagica(GetComponent<CartaMagica>());
                objSelecionada.SetActive(true);
            }
            else
            {
                gerenciador.receberCartaMagica(GetComponent<CartaMagica>(), false);
            }
            //personagem3d.GetComponent<Animator>().SetTrigger("PoseAtacar");
            //gerenciador.client.Send("SELECIONARATACANTE|" + nome);
        }
    }
    private void OnMouseEnter()
    {
       
    }
    public void TirarSelecao()
    {
        sendoUsada = false;
        //personagem3d.GetComponent<Animator>().SetTrigger("PoseAtacarParar");
        objSelecionada.SetActive(false);
    }
    void SelecionarDeck()
    {
        ChecarExistencia(gameObject.name, true);
    }
    void RetirarSelecaoDeck()
    {
        ChecarExistencia(gameObject.name, false);
    }
    void ChecarExistencia(string nome, bool selecionar)
    {
        if (gerenciador.confirmados != 3)
        {
            Vector3 ps = posicaoNormal;
            ps.y += 2;
            posicaoSelecionada = ps;
            if (selecionar)
            {
                selecionando = true;
                if (gerenciador.cartaM1 == "")
                {
                    gerenciador.cartaM1 = nome;
                }
                else if (gerenciador.cartaM2 == "")
                {
                    gerenciador.cartaM2 = nome;
                }
                else if (gerenciador.cartaM3 == "")
                {
                    gerenciador.cartaM3 = nome;
                }
                else
                {
                    selecionando = false;
                }
                if (selecionando)
                {
                    transform.position = posicaoSelecionada;
                    selecionada = true;
                }
            }
            else
            {
                if (gerenciador.cartaM1 == nome)
                {
                    gerenciador.cartaM1 = "";
                }
                else if (gerenciador.cartaM2 == nome)
                {
                    gerenciador.cartaM2 = "";
                }
                else if (gerenciador.cartaM3 == nome)
                {
                    gerenciador.cartaM3 = "";
                }
                selecionando = false;
                selecionada = false;
                transform.position = posicaoNormal;
            }
        }
    }
    void SelecionarCartaAfetada()
    {
        cartaAfetada = gerenciador.cartaAtacante.GetComponent<Carta>();
    }
    public void UsarHabilidade()
    {
        Debug.Log("ENTROU NO SELECIONAR HABILIDADE");
        if (sendoUsada == false || quantidade <= 0)
        {
            return;
        }

        SelecionarCartaAfetada();

        //if (magia == 1)
        //{
        //    MagiaM1();
        //}
        //else if (magia == 2)
        //{
        //    MagiaM2();
        //}
        //else
        if (magia == 3)
        {
            MagiaM3();
        }
        else if (magia == 4)
        {
            MagiaM4();
        }
        //else if (magia == 5)
        //{
        //    MagiaM5();
        //}
        //else if (magia == 6)
        //{
        //    MagiaM6();
        //}
        sendoUsada = false;
        MudarQuantidade(-1);
    }
    public void MudarQuantidade(int q)
    {
        quantidade += q;
        if (quantidade < 1)
        {
            materialMeu.GetComponent<MeshRenderer>().enabled = false;
        }
        else if (materialMeu.GetComponent<MeshRenderer>().enabled == false && quantidade < 2)
        {
            materialMeu.GetComponent<MeshRenderer>().enabled = true;
        }
        if (q > 0)
        {
            if (curarObj1.active == false && magia == 1)
            {
                curarObj1.SetActive(true);
                curarObj2.SetActive(true);
                curarObj3.SetActive(true);
            }
            else if (ressuscitarObj1.active == false && magia == 5)
            {
                ressuscitarObj1.SetActive(true);
                ressuscitarObj2.SetActive(true);
                ressuscitarObj3.SetActive(true);
            }
            else if (gerenciador.cartaObj1.botaoDanoInstantaneom != null)
            { if (gerenciador.cartaObj1.botaoDanoInstantaneom.active == false && magia == 2)
                {
                    gerenciador.cartaObj1.botaoDanoInstantaneom.SetActive(true);
                    gerenciador.cartaObj2.botaoDanoInstantaneom.SetActive(true);
                    gerenciador.cartaObj3.botaoDanoInstantaneom.SetActive(true);
                }
            }
        }
        if (q > 0 && quantidade > 1)
        {
            Vector3 posicaoUltCarta;
            if (cartasSobrando.Count > 0)
            {
                posicaoUltCarta = cartasSobrando[cartasSobrando.Count - 1].transform.position;
            }
            else
            {
                posicaoUltCarta = materialMeu.transform.position;
            }
            posicaoUltCarta.x = posicaoUltCarta.x - .5f;
            posicaoUltCarta.y = posicaoUltCarta.y + 0.2f;
            Transform newTransform;
            GameObject newObject;
            if (cartasSobrando.Count > 0)
            {
                newTransform = cartasSobrando[cartasSobrando.Count - 1].transform;
                newObject = cartasSobrando[cartasSobrando.Count - 1];
            }
            else
            {
                newTransform = materialMeu.transform;
                newObject = materialMeu;
            }

            cartasSobrando.Add(Instantiate(newObject, posicaoUltCarta, newTransform.rotation, newTransform));
        }
        else if (q < 0 && cartasSobrando.Count > 0)
        {
            if (cartasSobrando.Count > 0)
            {
                Destroy(cartasSobrando[cartasSobrando.Count - 1]);
                cartasSobrando.RemoveAt(cartasSobrando.Count - 1);
            }
        }
    }
    public void MagiaM1()
    {
        cartaAfetada.RecuperarVida(200);
    }
    void MagiaM2()
    {

    }
    void MagiaM3()
    {
        Debug.Log("ENTROU NA MAGIA 3");
        gerenciador.cartaAtacante.GetComponent<Carta>().AumentarAtaque();
    }
    void MagiaM4()
    {
        gerenciador.cartaAtacante.GetComponent<Carta>().AumentarAgilidade();
    }
    void MagiaM5()
    {

    }
    void MagiaM6()
    {

    }
    public void SelecionarCartaMagica(string n)
    {
        if (n == "CartaM1") //CURAR 200
        {
            magia = 1;
            nome = "magia1";
            materialMeu.GetComponent<MeshRenderer>().material = GameObject.Find("CartaM1").GetComponent<MeshRenderer>().material;
            curarObj1.SetActive(true);
            curarObj2.SetActive(true);
            curarObj3.SetActive(true);
        }
        else if (n == "CartaM2") //DANO IMADIATO
        {
            magia = 2;
            nome = "magia2";
            materialMeu.GetComponent<MeshRenderer>().material = GameObject.Find("CartaM2").GetComponent<MeshRenderer>().material;
            gerenciador.cartaIObj1.botaoDanoInstantaneom.SetActive(true);
            gerenciador.cartaIObj2.botaoDanoInstantaneom.SetActive(true);
            gerenciador.cartaIObj3.botaoDanoInstantaneom.SetActive(true);
        }
        else if (n == "CartaM3") //FURIA AUMENTA ATAQUE EM 250 
        {
            magia = 3;
            nome = "magia3";
            materialMeu.GetComponent<MeshRenderer>().material = GameObject.Find("CartaM3").GetComponent<MeshRenderer>().material;
        }
        else if (n == "CartaM4") //MAIS 1 DE AGILIDADE
        {
            magia = 4;
            nome = "magia4";
            materialMeu.GetComponent<MeshRenderer>().material = GameObject.Find("CartaM4").GetComponent<MeshRenderer>().material;
        }
        else if (n == "CartaM5") //REVIVER COM 300 DE VIDA
        {
            magia = 5;
            nome = "magia5";
            materialMeu.GetComponent<MeshRenderer>().material = GameObject.Find("CartaM5").GetComponent<MeshRenderer>().material;
            ressuscitarObj1.SetActive(true);
            ressuscitarObj2.SetActive(true);
            ressuscitarObj3.SetActive(true);
        }
        else if (n == "CartaM6") //ATAQUE DUPLO
        {
            magia = 6;
            nome = "magia6";
            materialMeu.GetComponent<MeshRenderer>().material = GameObject.Find("CartaM6").GetComponent<MeshRenderer>().material;
        }
    }
}
