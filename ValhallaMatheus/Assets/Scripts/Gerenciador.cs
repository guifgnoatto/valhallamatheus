﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.UI;

public class Gerenciador : MonoBehaviour
{
    //FASE DE ESCOLHA DE CARTAS
    public GameObject deck, deckMagico, test, botaoAtaque;
    public int confirmados;
    public string carta1, carta2, carta3, cartaI1, cartaI2, cartaI3, cartaM1, cartaM2, cartaM3;
    bool selecionouMagicas;
    //GAMEPLAY
    public GameObject minhasCartas, cartasInimigos, cartasMagicas, cartaAtacante, cartaAtacada, botaoTurno, vidasCartas;
    public CartaMagica cartaUsada;
    public Client client;
    public Carta cartaObj1, cartaObj2, cartaObj3, cartaIObj1, cartaIObj2, cartaIObj3;
    public CartaMagica cartaObjM1, cartaObjM2, cartaObjM3, cartaMagicaAtual1, cartaMagicaAtual2, cartaMagicaAtual3;
    public int turno, meuTurno;
    bool usouCartaMagica;
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public GameObject minhaChama, chamaInimigo, tutorial;
    public GameObject telaDerrota, telaVitoria;
    void Start()
    {
        carta1 = "";
        carta2 = "";
        carta3 = "";
        deck = GameObject.Find("Deck");
        turno = 1;
        meuTurno = 0;
        selecionouMagicas = false;
        //Cursor.SetCursor(cursorTexture, Vector2.zero, cursorMode);
        client = GameObject.FindObjectOfType<Client>();
        client.gerenciador = GetComponent<Gerenciador>();
        usouCartaMagica = false;
    }
    public void usarCartaMagica()
    {
        usouCartaMagica = true;
        if (cartaMagicaAtual1 != null)
        {
            cartaMagicaAtual1.UsarHabilidade();
        }
        if (cartaMagicaAtual2 != null)
        {
            cartaMagicaAtual2.UsarHabilidade();
        }
        if (cartaMagicaAtual3 != null)
        {
            cartaMagicaAtual3.UsarHabilidade();
        }
    }
    void Update()
    {
        if (confirmados == 2)
        {
            AnimacaoCartasInimigo();
            AnimacaoCartasInimigoVirar();
            botaoAtaque.SetActive(true);
            botaoTurno.SetActive(true);
            //vidasCartas.SetActive(true);
            confirmados = 3;
            tutorial.SetActive(true);
        }
        if (confirmados == 3)
        {
            if (cartaObj1.gameObject.active == false && cartaObj2.gameObject.active == false && cartaObj3.gameObject.active == false)
            {
                telaDerrota.SetActive(true);
            }
            else if (cartaIObj1.gameObject.active == false && cartaIObj2.gameObject.active == false && cartaIObj3.gameObject.active == false)
            {
                telaVitoria.SetActive(true);
            }
        }
    }
    public void MudarAtacante(GameObject ata)
    {
        string n;
        if (cartaAtacante != null)
        {
            cartaAtacante.GetComponent<Carta>().TirarSelecao();
        }
        if (ata == null)
        {
            n = "";
        }
        else
        {
            n = ata.GetComponent<Carta>().nome;
        }
        cartaAtacante = AcharAtacante(n);
    }
    GameObject AcharAtacante(string ata)
    {
        if (turno == meuTurno)
        {
            if (ata == cartaObj1.nome)
            {
                return cartaObj1.gameObject;
            }
            else if (ata == cartaObj2.nome)
            {
                return cartaObj2.gameObject;
            }
            else if (ata == cartaObj3.nome)
            {
                return cartaObj3.gameObject;
            }
            return null;

        }
        else
        {
            if (ata == cartaIObj1.nome)
            {
                return cartaIObj1.gameObject;
            }
            else if (ata == cartaIObj2.nome)
            {
                return cartaIObj2.gameObject;
            }
            else if (ata == cartaIObj3.nome)
            {
                return cartaIObj3.gameObject;
            }
            return null;
        }
    }
    public void MudarAtacado(GameObject ata)
    {
        string n;
        if (cartaAtacada != null)
        {
            cartaAtacada.GetComponent<Carta>().TirarSelecao();
        }
        if (ata == null)
        {
            n = "";
        }
        else
        {
            n = ata.GetComponent<Carta>().nome;
        }
        cartaAtacada = AcharAtacada(n);
    }
    GameObject AcharAtacada(string ata)
    {
        if (turno == meuTurno)
        {
            if (ata == cartaIObj1.nome)
            {
                return cartaIObj1.gameObject;
            }
            else if (ata == cartaIObj2.nome)
            {
                return cartaIObj2.gameObject;
            }
            else if (ata == cartaIObj3.nome)
            {
                return cartaIObj3.gameObject;
            }
            return null;
        }
        else
        {
            if (ata == cartaObj1.nome)
            {
                return cartaObj1.gameObject;
            }
            else if (ata == cartaObj2.nome)
            {
                return cartaObj2.gameObject;
            }
            else if (ata == cartaObj3.nome)
            {
                return cartaObj3.gameObject;
            }
            return null;
        }
    }
    public void ReceberDanoInstantaneo(string personagem)
    {
        if (cartaObj1.nome == personagem)
        {
            cartaObj1.TomarDano(200);
        }
        else if (cartaObj2.nome == personagem)
        {
            cartaObj2.TomarDano(200);
        }
        else if (cartaObj3.nome == personagem)
        {
            cartaObj3.TomarDano(200);
        }
    }
    public void receberCartaMagica(CartaMagica c, bool selecionar = true)
    {
        if (selecionar)
        {
            if (cartaMagicaAtual1 == null)
            {
                cartaMagicaAtual1 = c;
                cartaMagicaAtual1.setSendoUsada(true);
            }
            else if (cartaMagicaAtual2 == null)
            {
                cartaMagicaAtual2 = c;
                cartaMagicaAtual2.setSendoUsada(true);
            }
            else if (cartaMagicaAtual3 == null)
            {
                cartaMagicaAtual3 = c;
                cartaMagicaAtual3.setSendoUsada(true);

            }

            //if (cartaMagicaAtual1 != null)
            //{
            //    cartaMagicaAtual1.TirarSelecao();
            //}
            //cartaMagicaAtual1 = c;
            //if (cartaMagicaAtual1 != null)
            //{
            //    cartaMagicaAtual1.setSendoUsada(true);
            //}
        }
        else
        {
            if (cartaMagicaAtual1 != null && c.nome == cartaMagicaAtual1.nome)
            {
                cartaMagicaAtual1.TirarSelecao();
                cartaMagicaAtual1 = null;
            }
            else if (cartaMagicaAtual2 != null && c.nome == cartaMagicaAtual2.nome)
            {
                cartaMagicaAtual2.TirarSelecao();
                cartaMagicaAtual2 = null;
            }
            else if (cartaMagicaAtual3 != null && c.nome == cartaMagicaAtual3.nome)
            {
                cartaMagicaAtual3.TirarSelecao();
                cartaMagicaAtual3 = null;
            }
        }
    }
    public void ReceberAtacante(string ata, bool selecionar = true)
    {
        if (selecionar)
        {
            if (cartaAtacante != null)
            {
                cartaAtacante.GetComponent<Carta>().TirarSelecao();
            }
            cartaAtacante = AcharAtacante(ata);
            if (cartaAtacante != null)
            {
                cartaAtacante.GetComponent<Carta>().SelecionarAtaque();
            }
        }
        else
        {
            cartaAtacante.GetComponent<Carta>().TirarSelecao();
            cartaAtacante = null;
        }
    }
    public void ReceberAtacada(string ata, bool selecionar = true)
    {
        if (selecionar)
        {
            if (cartaAtacada != null)
            {
                cartaAtacada.GetComponent<Carta>().TirarSelecao();
            }
            cartaAtacada = AcharAtacada(ata);
            if (cartaAtacada != null)
            {
                cartaAtacada.GetComponent<Carta>().SelecionarAtaque();
            }
        }
        else
        {
            cartaAtacada.GetComponent<Carta>().TirarSelecao();
            cartaAtacada = null;
        }
    }
    public void BotaoAtacar()
    {
        if (cartaAtacante != null && cartaAtacada != null)
        {
            ChecaEfeitoMagico();
            EnviarCombate();
        }
    }
    void ChecaEfeitoMagico()
    {
        if (cartaMagicaAtual1 != null)
        {
            cartaMagicaAtual1.UsarHabilidade();
        }
        if (cartaMagicaAtual2 != null)
        {
            cartaMagicaAtual2.UsarHabilidade();
        }
        if (cartaMagicaAtual3 != null)
        {
            cartaMagicaAtual3.UsarHabilidade();
        }
    }
    void EnviarCombate()
    {
        usarCartaMagica();
        EnviarParaCliente();
    }
    void EnviarParaCliente()
    {
        Carta nome1 = cartaAtacante.GetComponent<Carta>();
        Carta nome2 = cartaAtacada.GetComponent<Carta>();
        client.Send("ATAQUE|" + nome1.nome + "|" + nome1.agilidade + "|" + nome2.nome + "|" + nome2.agilidade + "|" + nome1.ataque);
        client.Send("ATAQUE2|" + cartaObj1.vida + "|" + cartaObj2.vida + "|" + cartaObj3.vida);
    }
    public void RecebeCombate(string n1, string n2, int d1, int d2, int a)
    {
        if (turno != meuTurno)
        {
            cartaAtacante = AcharAtacante(n1);
            cartaAtacada = AcharAtacada(n2);
        }
        RecebeDados(d1, d2, a);
    }
    public void RecebeMagiaVida(int v1, int v2, int v3)
    {
        cartaIObj1.vida = v1;
        cartaIObj2.vida = v2;
        cartaIObj3.vida = v3;
        cartaIObj1.AtualizarCanvas();
        cartaIObj2.AtualizarCanvas();
        cartaIObj3.AtualizarCanvas();
        cartaIObj1.AtualizarHealthbar(0);//healthSlider.value = cartaIObj1.vida;
        cartaIObj2.AtualizarHealthbar(0);//healthSlider.value = cartaIObj2.vida;
        cartaIObj3.AtualizarHealthbar(0);//healthSlider.value = cartaIObj3.vida;
    }
    public void EnviarCuras(int quant)
    {
        client.Send("RECUPERARVIDA|" + cartaObj1.vida + "|" + cartaObj2.vida + "|" + cartaObj3.vida);
        if (quant < 1)
        {
            cartaObj1.botaoCura.SetActive(false);
            cartaObj2.botaoCura.SetActive(false);
            cartaObj3.botaoCura.SetActive(false);
        }
    }
    public void EnviarReviver(int quant, string cartaRevivida)
    {
        client.Send("REVIVERPERSONAGEM|" + cartaRevivida);
        if (quant < 1)
        {
            cartaObj1.botaoReviver.SetActive(false);
            cartaObj2.botaoReviver.SetActive(false);
            cartaObj3.botaoReviver.SetActive(false);
        }
    }
    public void ReviverInimigo(string nomeCarta)
    {
        if (cartaIObj1.nome == nomeCarta)
        {
            cartaIObj1.AtivarCarta();
            cartaObj1.Reviver();
        }
        else if (cartaIObj2.nome == nomeCarta)
        {
            cartaIObj2.AtivarCarta();
        }
        else if (cartaIObj3.nome == nomeCarta)
        {
            cartaIObj3.AtivarCarta();
        }

    }
    public void RecebeDados(int d1, int d2, int a)
    {
        Carta c1 = cartaAtacante.GetComponent<Carta>();
        Carta c2 = cartaAtacada.GetComponent<Carta>();
        c1.ataque = a;
        if (d1 > d2)
        {
            c2.TomarDano(c1.ataque);
        }
        else if (d1 == d2)
        {
            c1.TomarDano(c2.ataque);
        }
        ResetarCombatentes();
    }
    public void EncerrarTurno()
    {
        if (meuTurno == turno)
        {
            client.Send("FIMTURNO|" + meuTurno);
            if (meuTurno == 1)
                turno = 2;
            else
                turno = 1;
            botaoTurno.GetComponent<Button>().interactable = false;
        }
    }
    void ResetarCombatentes()
    {
        cartaAtacante.GetComponent<Carta>().ResetarTurno();
        cartaAtacada.GetComponent<Carta>().ResetarTurno();
        cartaAtacante.GetComponent<Carta>().TirarSelecao();
        cartaAtacada.GetComponent<Carta>().TirarSelecao();
        if (cartaMagicaAtual1 != null)
        {
            cartaMagicaAtual1.TirarSelecao();
        }
        if (cartaMagicaAtual2 != null)
            cartaMagicaAtual2.TirarSelecao();
        if (cartaMagicaAtual3 != null)
            cartaMagicaAtual3.TirarSelecao();
        cartaAtacante = null;
        cartaAtacada = null;
        cartaObj1.atacante = false;
        cartaObj2.atacante = false;
        cartaObj3.atacante = false;
        cartaIObj1.atacante = false;
        cartaIObj2.atacante = false;
        cartaIObj3.atacante = false;
        usouCartaMagica = false;
    }
    public void RecebeTurno(int t = -1)
    {
        if (t != -1)
        {
            meuTurno = t;
            //test.GetComponentInChildren<Text>().text = meuTurno.ToString();
            botaoTurno.GetComponentInChildren<Text>().text = t.ToString();
        }
        else
        {
            if (turno == 1)
            {
                turno = 2;
            }
            else
            {
                turno = 1;
            }
            if (turno == meuTurno)
            {
                ReceberMagiaAleatoria();
            }
        }
        if (turno == meuTurno)
        {
            botaoTurno.GetComponent<Button>().interactable = true;
            botaoAtaque.GetComponent<Button>().interactable = true;
            chamaInimigo.SetActive(false);
            minhaChama.SetActive(true);
        }
        else
        {
            botaoTurno.GetComponent<Button>().interactable = false;
            botaoAtaque.GetComponent<Button>().interactable = false;
            chamaInimigo.SetActive(true);
            minhaChama.SetActive(false);
        }
    }
    void ReceberMagiaAleatoria()
    {
        int num = UnityEngine.Random.Range(1, 4);
        if (num == 1)
        {
            cartaObjM1.MudarQuantidade(1);
        }
        else if (num == 2)
        {
            cartaObjM2.MudarQuantidade(1);
        }
        else
        {
            cartaObjM3.MudarQuantidade(1);
        }
    }
    //DAQUI PRA BAIXO SÓ FUNÇÕES RELACIONADAS A ESCOLHA DE CARTAS
    public void EnviarMinhasCartas(bool pular = false)
    {
        if (pular)
            selecionouMagicas = true;
        if (selecionouMagicas)
        {
            if ((carta1 != "" && carta2 != "" && carta3 != "" && cartaM1 != "" && cartaM2 != "" && cartaM3 != "") || (pular == true))
            {
                confirmados += 1;
                AnimacaoCartasMinhas();
                AnimacaoCartasMinhasVirar();
                MontarStatus(carta1, carta2, carta3, true);
                MontarStatusMagicas(cartaM1, cartaM2, cartaM3);
                deckMagico.transform.position = new Vector3(deck.transform.position.x, deck.transform.position.y + 100, deck.transform.position.z);
                //deck.SetActive(false);
                GameObject.Find("BotaoConfirmaCartas").SetActive(false);
                client.Send("MYCARDS|" + carta1 + "|" + carta2 + "|" + carta3);
            }
        }
        else
        {
            deck.transform.position = new Vector3(deck.transform.position.x, deck.transform.position.y + 100, deck.transform.position.z);
            selecionouMagicas = true;
            deckMagico.SetActive(true);
        }
    }
    public void AnimacaoCartasInimigo()
    {
        cartasInimigos.GetComponent<Animator>().SetTrigger("POSICIONAR_CARTAS");
    }
    public void AnimacaoCartasInimigoVirar()
    {
        cartasInimigos.GetComponent<Animator>().SetBool("VIRAR", true);
    }
    public void AnimacaoCartasMinhas()
    {
        minhasCartas.GetComponent<Animator>().SetTrigger("POSICIONAR_CARTAS");
        cartasMagicas.GetComponent<Animator>().SetTrigger("Entrar");
    }
    public void AnimacaoCartasMinhasVirar()
    {
        minhasCartas.GetComponent<Animator>().SetBool("VIRAR", true);
    }
    public void ReceberCartasInimigo()
    {
        AnimacaoCartasInimigo();
        MontarStatus(cartaI1, cartaI2, cartaI3, false);
    }
    public void MontarStatus(string c1, string c2, string c3, bool aliado)
    {
        string carta = c1;

        for (int i = 0; i < 3; i++)
        {
            Debug.Log(carta);
            int vida = 0, ataque = 0, agilidade = 0;
            if (carta == "Arqueiro 1")
            {
                vida = 300;
                ataque = 300;
                agilidade = 3;
            }
            else if (carta == "Arqueiro 2")
            {
                vida = 400;
                ataque = 200;
                agilidade = 3;
            }
            else if (carta == "Guerreiro 1")
            {
                vida = 1000;
                ataque = 800;
                agilidade = 1;
            }
            else if (carta == "Guerreiro 2")
            {
                vida = 1200;
                ataque = 600;
                agilidade = 1;
            }
            else if (carta == "Mago 1")
            {
                vida = 600;
                ataque = 400;
                agilidade = 2;
            }
            else if (carta == "Mago 2")
            {
                vida = 500;
                ataque = 500;
                agilidade = 2;
            }

            if (carta == c1)
            {
                if (aliado)
                {
                    cartaObj1.GetComponent<Carta>().ReceberStatus(vida, ataque, agilidade, carta, true, GameObject.Find(carta).GetComponent<MeshRenderer>());// GameObject.Find("HealthbarA1"));
                    carta = c2;
                }
                else
                {
                    cartaIObj1.GetComponent<Carta>().ReceberStatus(vida, ataque, agilidade, carta, false, GameObject.Find(carta).GetComponent<MeshRenderer>());// GameObject.Find("HealthbarI1"));
                    carta = c2;
                }
            }
            else if (carta == c2)
            {
                if (aliado)
                {
                    cartaObj2.GetComponent<Carta>().ReceberStatus(vida, ataque, agilidade, carta, true, GameObject.Find(carta).GetComponent<MeshRenderer>());// GameObject.Find("HealthbarA2"));
                    carta = c3;
                }
                else
                {
                    cartaIObj2.GetComponent<Carta>().ReceberStatus(vida, ataque, agilidade, carta, false, GameObject.Find(carta).GetComponent<MeshRenderer>());// GameObject.Find("HealthbarI2"));
                }
                carta = c3;
            }
            else
            {
                if (aliado)
                {
                    cartaObj3.GetComponent<Carta>().ReceberStatus(vida, ataque, agilidade, carta, true, GameObject.Find(carta).GetComponent<MeshRenderer>());// GameObject.Find("HealthbarA3"));
                }
                else
                {
                    cartaIObj3.GetComponent<Carta>().ReceberStatus(vida, ataque, agilidade, carta, false, GameObject.Find(carta).GetComponent<MeshRenderer>());// GameObject.Find("HealthbarI3"));
                }
            }
        }
    }
    public void MontarStatusMagicas(string c1, string c2, string c3)
    {
        cartaObjM1.SelecionarCartaMagica(c1);
        cartaObjM2.SelecionarCartaMagica(c2);
        cartaObjM3.SelecionarCartaMagica(c3);
    }

}
