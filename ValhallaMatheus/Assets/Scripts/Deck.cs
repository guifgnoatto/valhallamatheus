﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{
    // Start is called before the first frame update
    Vector3 posicaoInicial, posicaoFinal;
    [Range(0f,5f)]
    public float tempo;
    [Range(0f, 1f)]
    float porcCompleta;

    public bool movimentar;
    void Start()
    {
        tempo = .5f;
        movimentar = false;
        posicaoInicial = transform.position;
        posicaoInicial.x = 9;
        transform.position = posicaoInicial;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
            if (transform.position.x < 9f)
            {
                movimentar = true;
                posicaoInicial = transform.position;
                Vector3 posf = posicaoInicial;
                posf.x += 3.88f;
                posicaoInicial = posf;
                MoveToPosition();
            }
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            if (transform.position.x > -14.28f)
            {
                movimentar = true;
                posicaoInicial = transform.position;
                Vector3 posf = posicaoInicial;
                posf.x -= 3.88f;
                posicaoInicial = posf;
                MoveToPosition();

            }
        }

    }

    void MoveToPosition()
    {
        if (movimentar == true)
            transform.position = posicaoInicial;
        movimentar = false;
    }
}
