﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class NoSelectButton : MonoBehaviour
{
    Button b;
    bool selected;
    void Start()
    {
        b = gameObject.GetComponent<Button>();
        selected = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (!selected)
            return;

        if (EventSystem.current.currentSelectedGameObject == gameObject)
        {
            EventSystem.current.SetSelectedGameObject(null);
        }
    }
    private void OnMouseOver()
    {
        selected = true;
    }
}
