﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateWarriors : MonoBehaviour
{
    public GameObject carta1, carta2, carta3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void InvocarCartas()
    {
        carta1.GetComponent<Carta>().VerifyIfRender();
        carta2.GetComponent<Carta>().VerifyIfRender();
        carta3.GetComponent<Carta>().VerifyIfRender();
        carta1.GetComponent<Carta>().healthSlider.gameObject.SetActive(true);
        carta2.GetComponent<Carta>().healthSlider.gameObject.SetActive(true);
        carta3.GetComponent<Carta>().healthSlider.gameObject.SetActive(true);
        carta1.GetComponent<Carta>().canvas.SetActive(true);
        carta2.GetComponent<Carta>().canvas.SetActive(true);
        carta3.GetComponent<Carta>().canvas.SetActive(true);
    }
}
